# TPFB-12_dev_All DOCKER

## 1. Встановлення Docker

Завантажте та встановіть Docker на свій комп'ютер (Get Docker ).
Переконайтеся, що Docker Engine запущено і працює.

![Docker engine is running](./public/readMe/Screenshot_1.png)

## 2. Ознайомлення з Docker CLI

Вивчіть основні команди Docker CLI (docker run, docker build, docker pull, docker ps, docker images і т.д.).
Виконайте команду "docker --version" та переконайтеся, що Docker встановлено правильно.

[Документація](https://docs.docker.com/engine/reference/commandline/cli/)

![Docker version](./public/readMe/Screenshot_2.png)

## 3. Створення Dockerfile:

Створіть текстовий файл з назвою "Dockerfile".
В Dockerfile визначте базовий образ (наприклад, Ubuntu, Debian, або інший) та встановіть в ньому веб-сервер (наприклад, Apache або Nginx).
Додайте необхідні команди для налаштування веб-сервера.

![Docker file](./public/readMe/Screenshot_3.png)

## 4. Будівництво та запуск контейнера:

Використовуючи Docker CLI, збудуйте образ з Dockerfile.
Запустіть контейнер на основі створеного образу.
Переконайтеся, що веб-сервер працює, і можна відкрити сторінку в браузері за адресою http://localhost.

[Документація](https://docs.docker.com/get-started/02_our_app/)

![Command line](./public/readMe/Screenshot_4.png)

![Result](./public/readMe/Screenshot_5.png)

## 5. Редагування контейнера:

Відредагуйте вміст веб-сайту, який розгорнувся в контейнері.
Перебудуйте образ та перезапустіть контейнер, щоб побачити зміни.

[Документація](https://docs.docker.com/get-started/03_updating_app/)

![Edit container result](./public/readMe/Screenshot_6.png)

## 6. Завершення завдання:

Документуйте весь процес виконання завдання, включаючи команди Docker CLI та кроки з Dockerfile.
Підготуйте короткий звіт, в якому вкажете, які ви навчилися під час виконання завдання та які труднощі виникли. Документацію додайте як гугл документ до опису цієї задачі, в документа мають бути скріншоти з поясненням того що Ви виконали

Тема для мене нова, але в цілому дуже зрозумілі офіційні доки, вийшло запустити контейнер майже одразу.
